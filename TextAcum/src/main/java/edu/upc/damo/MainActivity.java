package edu.upc.damo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    EditText camp;
    TextView res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        inicialitzaMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_ajut:
                mostraAjut();
                return true;
            case R.id.action_esborra:
                esborraResultat();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inicialitza() {
        res = findViewById(R.id.resultat);
    }

    private void inicialitzaMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_afegeix);

        programaFocus(menuItem);
        programaAccions(menuItem);
    }



    // Funcions auxiliars per a la programació dels menús


    private void mostraAjut(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
            .setTitle(R.string.app_name)
            .setMessage(getString(R.string.TextAjut))
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

         AlertDialog dialog = builder.create();

        dialog.show();
    }

       // Funcions auxiliars per a la programació dels menús

    private void programaAccions(MenuItem menuItem) {
        camp =  (EditText) menuItem
                .getActionView()
                .findViewById(R.id.entradaDada);
        camp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return nouContingut(v, actionId, event);
            }
        });
    }

    private void programaFocus(MenuItem menuItem) {
        // Les dues retro-crides han de retornar true perquè volem tractar

        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.expandActionView();
                return true;
            }
        });

        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                 camp.requestFocus();
                mostraTeclat(camp);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                amagaTeclat(camp);
                return true;
            }
        });
    }

    // Accions sobre la view executades com a resultat de la interacció



    private void esborraResultat() {
        res.setText("");
        camp.setText("");
    }


    private Teclat getInstanceTeclat(){
        if (hiHaTeclatHard())
            return new TeclatHard();
        else
            return new TeclatSoft();
    }

    private boolean hiHaTeclatHard() {
        return getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }


    private  abstract class Teclat {
        final static int  DESCONEGUT = 0;
        final static int  OK = 1;

        abstract int accio( int actionId, KeyEvent event);

    }

    private  class TeclatHard extends Teclat{
        @Override
        int accio(int actionId, KeyEvent event) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    return OK;
            }
            return DESCONEGUT;
        }
    }
    private  class TeclatSoft extends Teclat{
        @Override
        int accio(int actionId, KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_GO:
                    return OK;
            }
            return DESCONEGUT;
        }
    }




    private boolean nouContingut(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)

        Teclat t = getInstanceTeclat();

        switch (t.accio(actionId, event)) {
            case Teclat.OK:
                afegeixAResultat(camp.getText());
                camp.setText("");
                Toast.makeText(this,getString(R.string.afegirToast),Toast.LENGTH_SHORT).show();
                return true;
        }

        return true;
    }

    private void mostraTeclat(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(v,InputMethodManager.RESULT_SHOWN);
    }

    private void amagaTeclat(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(),InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    private void afegeixAResultat(Editable text) {
        res.append("\n");
        res.append(text);
    }
}



